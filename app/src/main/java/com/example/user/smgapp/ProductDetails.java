package com.example.user.smgapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProductDetails extends NavigationDrawer {
    TextView app_title, p_det_priceTxt_1, p_det_priceTxt_2, p_det_priceTxt_3, p_det_price_1, p_det_price_2, p_det_price_3;
    Typeface face;
    String passed_id, url_ProductList, availability_value, review_url, wishList_url, remove_wishList_url;
    //ProgressDialog pDialog;
    String p_id, p_sku, p_name, p_img, p_price, p_description, special_price, product_price_single_txt;
    TextView tv_pName, tv_pPrice, tv_pDescriptn, tv_pPrice_strikeout, salesPkg_des, tv_product_price_single_txt;//, product_name_add_details, sku_txt_value
    ImageView im_pImage, more1, more2, more3, arrow_decscription;
    TableLayout table_additonl, table_review, table_det_pg_price;
    LinearLayout divdr_review_pink;
    TextView statc_howUrate, statc_rating, stock_txt, mandratory_txt, rating_alert_txt, delvry_validatn_txt, user_total_reviews_txt;
    RatingBar rating_bar;
    TextInputLayout userReview;
    TextView submit_mandratory_txt, submit_rating_alert, total_reviews_txt;
    ImageView close_review_dialog, p_det_wishlist;
    EditText userReview_edittxt, tv_pincode, review_submit_editText;
    Button submit_review, gift_it;
    RelativeLayout relative_ratebar, show_hide, wt_outer, flvr_outer;
    RatingBar ratebar, ratingBar_userReviews, submit_user_review_ratebar;
    int availability;
    LinearLayout reviews_layout;
    ProgressBar pBar;
    Gallery gallery;
    Button dialogloginbtn, dialogsignupbtn;
    //HashMap<String, String> images_hashmap;
    ArrayList<String> images_array, wt_title_array, wt_price_array, flvr_title_array, flvr_price_array;
    HashMap<String, String> images_hashmap;
    LinearLayout share_layout, addTocart;
    String user_review, p_validity, p_brand, p_seller_brand, p_flavour, is_perishable, submitted_user_review_string;
    String sales_packages_value, bangalore_price, delhi_price, mumbai_price, chennai_price;
    HomePage home;
    Spinner wt_spinner, flvr_spinner;
    LinkedHashMap<String, String> additnl_infr_map;
    AutoCompleteTextView delivryCity;

    ArrayList<HashMap<String, String>> getPinCodeandCityList;
    DBController controller = new DBController(this);
    int maxlen = 6;

    ArrayList<HashMap<String, String>> attrib_array;
    ArrayList<String> citieslist = new ArrayList<String>();

    HashMap<String, String> attribute_values;
    String atrib_label, attrib_val;
    String rating_percent;
    float rating_cal, rating_value, final_rating;
    int review_length;
    Dialog mDialog;
    ImageView cancel_dialog_login;
    Button loginhint, submit_review_btn;
    Dialog review_dialog;
    TextView txt7, txt10, txt12, txt20, textView4;
    String autocompletetext;
    String review_title, review_date, review, ovrl_rating, individual_rating, review_user_name;
    ArrayList<String> user_reviews_array = new ArrayList<>();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.product_details_page);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.product_details_page, null, false);
        drawer.addView(contentView, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        app_title = (TextView) findViewById(R.id.app_title);
        face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        getSupportActionBar().setTitle("");
        app_title.setTypeface(face);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Intent in = getIntent();
        passed_id = in.getStringExtra("product_id");
        SingletonActivity.productid = passed_id;
        gallery = (Gallery) findViewById(R.id.gallery1);
        p_det_wishlist = (ImageView) findViewById(R.id.p_det_wishlist);
        tv_pDescriptn = (TextView) findViewById(R.id.tv_det_descrptn);
        tv_pDescriptn.setTypeface(face);
        txt7 = (TextView) findViewById(R.id.textView7);
        txt10 = (TextView) findViewById(R.id.textView10);
        txt12 = (TextView) findViewById(R.id.textView12);
        txt20 = (TextView) findViewById(R.id.textView20);
        textView4 = (TextView) findViewById(R.id.textView4);
        txt7.setTypeface(face);
        txt10.setTypeface(face);
        txt12.setTypeface(face);
        txt20.setTypeface(face);
        textView4.setTypeface(face);

        p_det_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("customer id..", "customer id.." + SingletonActivity.custidstr);

                if (SingletonActivity.custidstr.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Ur not logged in,Please Login", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetails.this);
                    builder.setTitle("Alert!");
                    builder.setMessage("Please Login")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(getApplicationContext(), Login.class);
                                    startActivity(intent);
                                }


                            });
                    // Create the AlertDialog object and return it
                    builder.show();

                } else {
                    wishList_url = SingletonActivity.API_URL + "api/add_wishlist.php?customer_id=" + SingletonActivity.custidstr + "&product_id=" + passed_id;
                    remove_wishList_url = SingletonActivity.API_URL + "api/remove_item_wishlist.php?customerid=" + SingletonActivity.custidstr + "&productid=" + passed_id;
                    Log.e("wishlist api..", "wish list api.." + wishList_url);
                    Log.e("remove wishlist api..", "remove wish list api.." + remove_wishList_url);
                    v.setActivated(!v.isActivated());
                    if (v.isActivated()) {
                        Toast.makeText(getApplicationContext(), "addedd..", Toast.LENGTH_SHORT).show();
                        addTowishList(wishList_url);
                    } else {
                        Toast.makeText(getApplicationContext(), "removed....", Toast.LENGTH_SHORT).show();
                        removeFromwishList(remove_wishList_url);
                    }


                }
            }

        });


        //String passed_id="342";
        url_ProductList = SingletonActivity.API_URL + "api/product_details.php?product_id=" + passed_id;
        review_url = SingletonActivity.API_URL + "api/review.php?product_id=" + passed_id;


        Log.d("p_url", "product_list_url..." + url_ProductList);
        pBar = (ProgressBar) findViewById(R.id.progress_bar_custom);
        user_total_reviews_txt = (TextView) findViewById(R.id.total_reviews_txt);
        /*pDialog = new ProgressDialog(this);
        pDialog.setMessage("loading...");
        pDialog.setCancelable(false);*/
        pBar.setVisibility(View.VISIBLE);

        citieslist.clear();

        new Download().execute(url_ProductList);
        // getReview();

        System.out.println("CUST ID from singleton IN PRODUCT DETAILS ACTIVITY IS----" + SingletonActivity.custidstr);
        reviews_layout = (LinearLayout) findViewById(R.id.linearLayout);

        //reviewsFromUser();

        tv_pName = (TextView) findViewById(R.id.p_det_prName);
        tv_pName.setTypeface(face);
        tv_pPrice = (TextView) findViewById(R.id.p_det_price);
        total_reviews_txt = (TextView) findViewById(R.id.total_reviews_txt);
        total_reviews_txt.setTypeface(face);
        //tv_pDescriptn=(TextView)findViewById(R.id.p_det_prName);
        im_pImage = (ImageView) findViewById(R.id.p_det_prImg);
        tv_pName.setTypeface(face);
        tv_pPrice_strikeout = (TextView) findViewById(R.id.p_det_price_strikeout);
        tv_product_price_single_txt = (TextView) findViewById(R.id.product_price);
        tv_product_price_single_txt.setTypeface(face);

        stock_txt = (TextView) findViewById(R.id.stock_txt);
        stock_txt.setTypeface(face);
        tv_pDescriptn = (TextView) findViewById(R.id.tv_det_descrptn);
        /*product_name_add_details = (TextView) findViewById(R.id.product_name_add_details);
        sku_txt_value = (TextView) findViewById(R.id.sku_txt_value);*/
        ratebar = (RatingBar) findViewById(R.id.ratingBar);
        userReview_edittxt = (EditText) findViewById(R.id.userReview);
        mandratory_txt = (TextView) findViewById(R.id.mandratory_msg);
        rating_alert_txt = (TextView) findViewById(R.id.rating_alert);
        share_layout = (LinearLayout) findViewById(R.id.share_layout_tab);
        ratingBar_userReviews = (RatingBar) findViewById(R.id.total_review_rating);
       /* LayerDrawable stars = (LayerDrawable) ratingBar_userReviews.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);*/
        delvry_validatn_txt = (TextView) findViewById(R.id.delvry_validatn_txt);


        more1 = (ImageView) findViewById(R.id.more_img_1);
        more2 = (ImageView) findViewById(R.id.more_img_2);
        more3 = (ImageView) findViewById(R.id.img_more_3);
        arrow_decscription = (ImageView) findViewById(R.id.imageView5);

        salesPkg_des = (TextView) findViewById(R.id.more_details_1);
        salesPkg_des.setTypeface(face);
        table_additonl = (TableLayout) findViewById(R.id.det_table_additionl);
        table_review = (TableLayout) findViewById(R.id.tableRating);
        divdr_review_pink = (LinearLayout) findViewById(R.id.rating_pinkDivider);

        statc_howUrate = (TextView) findViewById(R.id.howTorateTXT);
        statc_rating = (TextView) findViewById(R.id.ratingTXT);

        relative_ratebar = (RelativeLayout) findViewById(R.id.relativeLayoutRatebar);
        rating_bar = (RatingBar) findViewById(R.id.ratingBar);
        Drawable stars1 = (Drawable) rating_bar.getProgressDrawable();
        stars1.setColorFilter(Color.parseColor("#CF0A8B"), PorterDuff.Mode.SRC_ATOP);
        userReview = (TextInputLayout) findViewById(R.id.input_layout_review);
        submit_review = (Button) findViewById(R.id.submit_review);
        show_hide = (RelativeLayout) findViewById(R.id.show_hide);
        gift_it = (Button) findViewById(R.id.btn_gift_it);
        gift_it.setTypeface(face);

        wt_spinner = (Spinner) findViewById(R.id.wt_spinner);
        flvr_spinner = (Spinner) findViewById(R.id.flvr_spinner);
        wt_outer = (RelativeLayout) findViewById(R.id.wt_outer);
        flvr_outer = (RelativeLayout) findViewById(R.id.flvr_outer);
        addTocart = (LinearLayout) findViewById(R.id.addTocart);

        citieslist = SingletonActivity.citiesarray;


        delivryCity = (AutoCompleteTextView) findViewById(R.id.delivryCity);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, SingletonActivity.citiesarray);
        delivryCity.setAdapter(adapter);

        delivryCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                autocompletetext = (String) parent.getItemAtPosition(position);
                //TODO Do something with the selected text
            }
        });

        p_det_priceTxt_1 = (TextView) findViewById(R.id.p_det_priceTxt_1);
        p_det_priceTxt_2 = (TextView) findViewById(R.id.p_det_priceTxt_2);
        p_det_priceTxt_3 = (TextView) findViewById(R.id.p_det_priceTxt_3);
        p_det_price_1 = (TextView) findViewById(R.id.p_det_price_1);
        p_det_price_2 = (TextView) findViewById(R.id.p_det_price_2);
        p_det_price_3 = (TextView) findViewById(R.id.p_det_price_3);
        table_det_pg_price = (TableLayout) findViewById(R.id.table_det_pg);
        tv_pincode = (EditText) findViewById(R.id.tv_det_pincode);


        tv_pincode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (tv_pincode.getText().toString().length() >= maxlen) {
                    String pincodestr = tv_pincode.getText().toString();
                    String cityfromdb = controller.getSingleCityEntry(pincodestr);

                    if (autocompletetext.equals(cityfromdb)) {
                        Toast.makeText(ProductDetails.this, "this city is available", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductDetails.this, "this city is unavailable", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });


        salesPkg_des.setVisibility(View.GONE);
        table_additonl.setVisibility(View.GONE);
        table_review.setVisibility(View.GONE);
        tv_pDescriptn.setVisibility(View.GONE);
        show_hide.setVisibility(View.GONE);
        gift_it.setVisibility(View.VISIBLE);

        more1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (salesPkg_des.getVisibility() == View.VISIBLE) {
                    salesPkg_des.setVisibility(View.GONE);
                    more1.setImageResource(R.drawable.down);
                } else {
                    salesPkg_des.setVisibility(View.VISIBLE);
                    more1.setImageResource(R.drawable.up);
                }


            }
        });
        more2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (table_additonl.getVisibility() == View.VISIBLE) {
                    table_additonl.setVisibility(View.GONE);
                    more2.setImageResource(R.drawable.down);
                } else {
                    table_additonl.setVisibility(View.VISIBLE);
                    more2.setImageResource(R.drawable.up);
                }


            }
        });
        more3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (table_review.getVisibility() == View.VISIBLE) {
                    table_review.setVisibility(View.GONE);
                    divdr_review_pink.setVisibility(View.GONE);
                    statc_howUrate.setVisibility(View.GONE);
                    relative_ratebar.setVisibility(View.GONE);
                    //rating_bar.setVisibility(View.GONE);
                    userReview.setVisibility(View.GONE);
                    submit_review.setVisibility(View.GONE);
                    mandratory_txt.setVisibility(View.GONE);
                    rating_alert_txt.setVisibility(View.GONE);
                    more3.setImageResource(R.drawable.down);
                } else {
                    table_review.setVisibility(View.VISIBLE);
                    divdr_review_pink.setVisibility(View.VISIBLE);
                    statc_howUrate.setVisibility(View.VISIBLE);
                    relative_ratebar.setVisibility(View.VISIBLE);
                    //rating_bar.setVisibility(View.VISIBLE);
                    userReview.setVisibility(View.VISIBLE);
                    submit_review.setVisibility(View.VISIBLE);
                    more3.setImageResource(R.drawable.up);
                }

            }
        });
        arrow_decscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_pDescriptn.getVisibility() == View.VISIBLE) {
                    tv_pDescriptn.setVisibility(View.GONE);
                    arrow_decscription.setImageResource(R.drawable.down);
                } else {
                    tv_pDescriptn.setVisibility(View.VISIBLE);
                    arrow_decscription.setImageResource(R.drawable.up);
                }
            }
        });
        gift_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                show_hide.setVisibility(View.VISIBLE);
                gift_it.setVisibility(View.GONE);

            }
        });


        share_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               /* Bitmap bitmap = takeScreenshot();
                saveBitmap(bitmap);*/

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);

                sendIntent.putExtra(Intent.EXTRA_TEXT, "check this product");
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share Via"));
            }
        });

        ratebar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                // Log.e("rating value...", "rating start are..." + String.valueOf(rating));
                Drawable drawable = ratingBar.getProgressDrawable();
                drawable.setColorFilter(Color.parseColor("#CF0A8B"), PorterDuff.Mode.SRC_ATOP);


            }
        });

        submit_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_review = userReview_edittxt.getText().toString();
                if (ratebar.getRating() == 0 && user_review.isEmpty() && user_review.equals("")) {
                    rating_alert_txt.setVisibility(View.VISIBLE);
                    mandratory_txt.setVisibility(View.VISIBLE);
                    // Log.e("condition 1..", "condition 1.." + user_review);

                } else if (ratebar.getRating() == 0) {
                    rating_alert_txt.setVisibility(View.VISIBLE);
                    mandratory_txt.setVisibility(View.INVISIBLE);
                    // Log.e("condition 2..", "condition 2.." + user_review);

                } else if (user_review.isEmpty() || user_review.equals("")) {
                    // Log.e("above conditions sucess", "above conditions sucess.." + "\t" + "user reviews..." + user_review);

                    rating_alert_txt.setVisibility(View.INVISIBLE);
                    mandratory_txt.setVisibility(View.VISIBLE);
                } else {
                    rating_alert_txt.setVisibility(View.INVISIBLE);
                    mandratory_txt.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Review Sumitted", Toast.LENGTH_SHORT).show();
                    userReview_edittxt.setText("");
                    ratebar.setRating(0F);
                }


            }
        });


        addTocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   if (is_perishable.equals("1")) {
                if (gift_it.getVisibility() == View.VISIBLE) {

                    show_hide.setVisibility(View.VISIBLE);
                    gift_it.setVisibility(View.GONE);

                }

                /*else if(delivryCity.getText().toString().isEmpty()||delivryCity.getText().toString().equals(""))
                {
                   // Toast.makeText(ProductDetails.this,"Choose delivery city..!",Toast.LENGTH_SHORT).show();
                    delivryCity.requestFocus();
                    delvry_validatn_txt.setVisibility(View.VISIBLE);
                    delvry_validatn_txt.setText("Choose delivery city..!");


                }*/


                //    if(SingletonActivity.custidstr==null) {

                else if (loginboolean == false) {


                    mDialog = new Dialog(ProductDetails.this, R.style.AppTheme);
                    mDialog.setCancelable(false);
                    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mDialog.setContentView(R.layout.productdetails_dialog);

                    cancel_dialog_login = (ImageView) mDialog.findViewById(R.id.cancel_logindialog);
                    dialogloginbtn = (Button) mDialog.findViewById(R.id.loginbtn);
                    dialogsignupbtn = (Button) mDialog.findViewById(R.id.signupbtn);

                    dialogloginbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ProductDetails.this, Login.class);
                            startActivity(intent);
                        }
                    });

                    dialogsignupbtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ProductDetails.this, RegistrationActivity.class);
                            startActivity(intent);
                        }
                    });


                    cancel_dialog_login.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            // TODO Auto-generated method stub
                            mDialog.dismiss();
                        }
                    });

                    mDialog.show();

                } else {
                    AddToCartVolleyApi();
                }


                // }

            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

   /* public  void removeFromwishList() {


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                remove_wishList_url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                try {
                    String name = response.getString("responseCode");
                    int response_code=Integer.parseInt(name);
                    Log.e("response code...","dlt response code.."+response_code);

                    Log.e("deleted from wishlist","deleted from wish list");

                    String message=response.getString("msg");
                   *//* if (message.contains("Removed Product From Wishlist Successfully")){


                    }*//*
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Request", "Error: " + error.getMessage());


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };

        GlobalClass.getInstance().addToRequestQueue(jsonObjReq);
    }
*/


    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    private void AddToCartVolleyApi() {

        // pBar.setVisibility(View.VISIBLE);


        // SingletonActivity.custidstr = "112";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SingletonActivity.API_URL + "api/add_to_cart.php?product_id=" + passed_id + "&&quantity=" + 1 + "&&customer_id=" + SingletonActivity.custidstr,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // pdia.dismiss();


                        System.out.println("RESPONSE IN ADD TO CART VOLLEY ACTIVITY IS" + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String isSuccessStr = jsonObject.getString("isSuccess");

                            String quotestr = jsonObject.getString("quote");

                            System.out.println("ISSUCCESS STRING IN ADD TO CART VOLLEY ACTIVITY IS" + isSuccessStr);
                            System.out.println("QUOTE STRING IN ADD TO CART VOLLEY ACTIVITY IS" + quotestr);

                            if (isSuccessStr.equalsIgnoreCase("true")) {
                                //   pBar.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(ProductDetails.this, CartActivity.class);
                                startActivity(intent);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProductDetails.this, error.toString(), Toast.LENGTH_LONG).show();
                        //  pdia.dismiss();
                        //  pBar.setVisibility(View.INVISIBLE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetails.this);
        requestQueue.add(stringRequest);
    }

    private void getReview() {
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                review_url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Request", response.toString());
                        try {
                            response.getString("isSuccess");
                            Log.e("response sucess..", "response sucess..");
                            JSONObject jobject = response.getJSONObject("result");
                            JSONArray array = jobject.getJSONArray("review");

                            JSONArray review_array = jobject.getJSONArray("rating_persent");
                            if (review_array != null) {
                                for (int i = 0; i < review_array.length(); i++) {
                                    String review_array_user = review_array.getString(i);
                                    Log.e(" rating..", "review_array_user rating..." + review_array_user);

                                    float individual_rating = (Float.parseFloat(review_array_user) / 100) * 5;
                                    Log.e("individual rating...", "individual rating..." + individual_rating);
                                }
                            }
                            if (array != null) {
                                for (int i = 0; i < array.length(); i++) {
                                    review_length = array.length();
                                    // JSONObject jobj=array.getJSONObject(i);
                                    user_total_reviews_txt.setText(String.valueOf(array.length()) + "\t" + "User Reviews");
                                    Log.e("user reviews text..", "user reviews text.." + user_total_reviews_txt.getText().toString());
                                    Log.e("array length..", "array length.." + array.length());
                                    JSONObject id = (JSONObject) array.get(i);
                                    String custom_review = id.getString("customer_name");
                                    String review = id.getString("review");

                                    Log.e("user review..", "user review.." + review);
                                }
                            }

                            rating_percent = jobject.getString("rating");
                            LayerDrawable stars = (LayerDrawable) ratingBar_userReviews.getProgressDrawable();
                            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                            rating_cal = Float.parseFloat(rating_percent);
                            rating_value = ((rating_cal / 100) * 5);

                            final_rating = (float) Math.round(rating_value * 100) / 100;
                            Log.e("rating percent..", "rating percent..." + rating_percent);
                            Log.e("rating_value..", "rating_value..." + final_rating);
                            // ratingBar_userReviews.setRating(Float.parseFloat(rating_percent));
                            ratingBar_userReviews.setRating(final_rating);


                            if (review_length == 0) {
                                user_total_reviews_txt.setText("Be the first to write review");
                            } else {
                                user_total_reviews_txt.setText(review_length + "\t" + "User Reviews");

                            }
                            reviews_layout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    Log.e("layout clicked..", "layout clicked....");
                                    if (user_total_reviews_txt.getText().toString() == "Be the first to write review") {

                                        review_dialog = new Dialog(ProductDetails.this, R.style.AppTheme);
                                        review_dialog.setCancelable(false);
                                        review_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        review_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                        review_dialog.setContentView(R.layout.custom);

                                        submit_review_btn = (Button) review_dialog.findViewById(R.id.submit_review_btn);
                                        submit_user_review_ratebar = (RatingBar) review_dialog.findViewById(R.id.review_submit_ratebar);
                                        review_submit_editText = (EditText) review_dialog.findViewById(R.id.review_sub_edittxt);
                                        submit_mandratory_txt = (TextView) review_dialog.findViewById(R.id.sumit_mandratory_msg);
                                        submit_rating_alert = (TextView) review_dialog.findViewById(R.id.submit_rating_alert);
                                        close_review_dialog = (ImageView) review_dialog.findViewById(R.id.review_submit_close);


                                        submit_user_review_ratebar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                                            @Override
                                            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                                                // Log.e("rating value...", "rating start are..." + String.valueOf(rating));
                                                Drawable drawable = ratingBar.getProgressDrawable();
                                                drawable.setColorFilter(Color.parseColor("#CF0A8B"), PorterDuff.Mode.SRC_ATOP);


                                            }
                                        });


                                        close_review_dialog.setOnClickListener(new View.OnClickListener() {

                                            @Override
                                            public void onClick(View arg0) {
                                                // TODO Auto-generated method stub
                                                review_dialog.dismiss();
                                            }
                                        });


                                        submit_review_btn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                submitted_user_review_string = review_submit_editText.getText().toString();
                                                if (submit_user_review_ratebar.getRating() == 0 && submitted_user_review_string.isEmpty() && submitted_user_review_string.equals("")) {
                                                    submit_rating_alert.setVisibility(View.VISIBLE);
                                                    submit_mandratory_txt.setVisibility(View.VISIBLE);
                                                    // Log.e("condition 1..", "condition 1.." + user_review);

                                                } else if (submit_user_review_ratebar.getRating() == 0) {
                                                    submit_rating_alert.setVisibility(View.VISIBLE);
                                                    submit_mandratory_txt.setVisibility(View.INVISIBLE);
                                                    // Log.e("condition 2..", "condition 2.." + user_review);

                                                } else if (submitted_user_review_string.isEmpty() || submitted_user_review_string.equals("")) {
                                                    // Log.e("above conditions sucess", "above conditions sucess.." + "\t" + "user reviews..." + user_review);

                                                    submit_rating_alert.setVisibility(View.INVISIBLE);
                                                    submit_mandratory_txt.setVisibility(View.VISIBLE);
                                                } else {
                                                    submit_rating_alert.setVisibility(View.INVISIBLE);
                                                    submit_mandratory_txt.setVisibility(View.INVISIBLE);
                                                    Toast.makeText(getApplicationContext(), "Review Sumitted", Toast.LENGTH_SHORT).show();
                                                    review_submit_editText.setText("");
                                                    submit_user_review_ratebar.setRating(0F);
                                                    review_dialog.dismiss();
                                                }


                                            }
                                        });


                                        review_dialog.show();


                                    } else {
                                        Log.e("Already reviews ..", "already reviews are there..");
                                        Intent intent = new Intent(getApplicationContext(), Reviews.class);
                                        intent.putExtra("review_user_name", review_user_name);
                                        intent.putExtra("review_date", review_date);
                                        intent.putExtra("review_title", review_title);

                                    }
                                }
                            });


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Request", "Error: " + error.getMessage());
                pBar.setVisibility(View.INVISIBLE);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
             /*   params.put("phone", phone);
                params.put("name", username);
                params.put("pwd",password);
                params.put("email", email);*/
                return params;
            }
        };

        GlobalClass.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ProductDetails Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.user.smgapp/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "ProductDetails Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.user.smgapp/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


    class Download extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // pDialog.show();
            pBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... f_url) {

            try {
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet(f_url[0]);
                HttpResponse response = client.execute(get);
                HttpEntity entity = response.getEntity();
                String jsonStr = EntityUtils.toString(entity);

                if (jsonStr != null) {
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    JSONObject jsonObjectResult = jsonObject.getJSONObject("result");
                    p_id = jsonObjectResult.getString("id");

                    p_name = jsonObjectResult.getString("name");
                    p_img = jsonObjectResult.getString("img");
                    p_price = jsonObjectResult.getString("price");
                    special_price = jsonObjectResult.getString("special_price");

                    p_description = jsonObjectResult.getString("description");
                    availability_value = jsonObjectResult.getString("availability");
                    //p_flavour  = jsonobj_additnl.getString("flavour");
                    if (jsonObjectResult.isNull("image_array")) {

                    } else {
                        JSONArray jarray = jsonObjectResult.getJSONArray("image_array");
                        images_array = new ArrayList<String>();
                        for (int i = 0; i < jarray.length(); i++) {
                            // images_hashmap = new HashMap<>();
                            JSONObject jsonobj_image = jarray.getJSONObject(i);
                            if (jsonobj_image != null) {
                                String image_urls = jsonobj_image.getString("images");
                                //images_hashmap.put("p_image_urls", image_urls);
                                images_array.add(image_urls);
                                // Log.e("images array...", "product details images..." + images_array);
                            }

                        }
                    }
                    sales_packages_value = jsonObjectResult.getString("sales_packages");
                    bangalore_price = "0";
                    delhi_price = "0";
                    mumbai_price = "0";
                    chennai_price = "0";

                    if (!jsonObjectResult.isNull("bangalore_price")) {
                        bangalore_price = jsonObjectResult.getString("bangalore_price");
                    }
                    if (!jsonObjectResult.isNull("delhi_price")) {
                        delhi_price = jsonObjectResult.getString("delhi_price");
                    }
                    if (!jsonObjectResult.isNull("mumbai_price")) {
                        mumbai_price = jsonObjectResult.getString("mumbai_price");
                    }
                    if (!jsonObjectResult.isNull("chennai_price")) {
                        chennai_price = jsonObjectResult.getString("chennai_price");
                    }


                    wt_title_array = new ArrayList<String>();
                    wt_price_array = new ArrayList<String>();

                    if (jsonObjectResult.isNull("weight")) {

                    } else {

                        JSONArray weight_jarray = jsonObjectResult.getJSONArray("weight");
                        if (weight_jarray != null) {
                            for (int i = 0; i < weight_jarray.length(); i++) {
                                JSONObject jsonobj_weight = weight_jarray.getJSONObject(i);
                                if (jsonobj_weight != null) {
                                    String wt_title = jsonobj_weight.getString("option_title");
                                    String wt_price = jsonobj_weight.getString("option_value_price");
                                    wt_title_array.add(wt_title);
                                    wt_price_array.add(wt_price);

                                }
                            }
                        }

                    }
                    flvr_title_array = new ArrayList<String>();
                    flvr_price_array = new ArrayList<String>();
                    if (jsonObjectResult.isNull("flavour_option")) {

                    } else {

                        JSONArray flavour_jarray = jsonObjectResult.getJSONArray("flavour_option");

                        if (flavour_jarray != null) {
                            for (int i = 0; i < flavour_jarray.length(); i++) {
                                JSONObject jsonobj_flavour = flavour_jarray.getJSONObject(i);
                                if (jsonobj_flavour != null) {
                                    String flvr_title = jsonobj_flavour.getString("option_title");
                                    String flvr_price = jsonobj_flavour.getString("option_value_price");
                                    flvr_title_array.add(flvr_title);
                                    flvr_price_array.add(flvr_price);

                                }
                            }
                        }
                    }

                    JSONArray jarrayAdditnl = jsonObjectResult.getJSONArray("additional_information");
                    additnl_infr_map = new LinkedHashMap<>();

                    if (jarrayAdditnl != null) {
                        attrib_array = new ArrayList<HashMap<String, String>>();


                        for (int i = 0; i < jarrayAdditnl.length(); i++) {


                            JSONObject jsonobj_add = jarrayAdditnl.getJSONObject(i);

                            attribute_values = new HashMap<String, String>();
                            if (jsonobj_add != null) {

                                atrib_label = jsonobj_add.getString("attributelabel");
                                attrib_val = jsonobj_add.getString("attributevalue");
                                if (!(attrib_val.equalsIgnoreCase("null")) && !(attrib_val.equalsIgnoreCase("false")) && !(attrib_val.equalsIgnoreCase("No"))) {
                                    //  ||!attrib_val.equalsIgnoreCase("false")||!attrib_val.equalsIgnoreCase("No")
                                    attribute_values.put("value", attrib_val);
                                    attribute_values.put("label", atrib_label);
                                    Log.e("attributes..", "attributes.." + attribute_values);
                                    attrib_array.add(attribute_values);

                                    Log.e("attributes..", "attributes array..." + attrib_array);

                                }
                                Log.e("attributes..", "attributes array1..." + attrib_array);

                            }

                        }


                    }
                    is_perishable = jsonObjectResult.getString("is_perishable");
                    Log.e("is perishanble...", "is perishable.." + is_perishable);


                    JSONObject review_obj = jsonObjectResult.getJSONObject("rivews");
                    if (review_obj != null) {


                        JSONArray indivl_rating = review_obj.getJSONArray("rating_persent");
                        if (indivl_rating != null) {
                            SingletonActivity.individual_rating_array = new ArrayList<>();

                            for (int i = 0; i < indivl_rating.length(); i++) {
                                individual_rating = indivl_rating.getString(i);
                                SingletonActivity.individual_rating_array.add(individual_rating);
                                String review_array_user = indivl_rating.getString(i);
                                float individual_rating = (Float.parseFloat(review_array_user) / 100) * 5;
                                Log.e("individual rating..", "individual rating array.." + SingletonActivity.individual_rating_array);
                            }
                        }


                        String review = review_obj.getString("review");
                        Log.e("review string..", "review string.." + review);
                        if (review.equals("null")) {

                            Log.e("no reviews..", "no reviews..");
                        } else {
                            JSONArray jarray_review = review_obj.getJSONArray("review");


                            Log.e("jarray review..", "jarray review.." + jarray_review);
                            if (jarray_review != null) {
                                SingletonActivity.rating_date_array = new ArrayList<>();
                                SingletonActivity.rating_name_array = new ArrayList<>();
                                SingletonActivity.rating_title_array = new ArrayList<>();
                                SingletonActivity.user_review_array = new ArrayList<>();


                                for (int i = 0; i < jarray_review.length(); i++) {

                                    JSONObject jsonobj_review = jarray_review.getJSONObject(i);
                                    review_length = jarray_review.length();

                                    if (jsonobj_review != null) {
                                        user_reviews_array = new ArrayList<>();
                                        review_title = jsonobj_review.getString("review_title");
                                        SingletonActivity.rating_title_array.add(review_title);
                                        review_date = jsonobj_review.getString("review_date");
                                        SingletonActivity.rating_date_array.add(review_date);
                                        review = jsonobj_review.getString("review");
                                        SingletonActivity.user_review_array.add(review);
                                        review_user_name = jsonobj_review.getString("customer_name");

                                        user_reviews_array.add(review_user_name);
                                        user_reviews_array.add(review_date);
                                        user_reviews_array.add(review_title);
                                        user_reviews_array.add(review);


                                        SingletonActivity.rating_name_array.add(review_user_name);
                                        Log.e("ratings...", "ratings.." + review_title + "\t" + review_date + "\n" + review);
                                    }

                                }
                                Log.e("user review array..","user_reviews_array.."+user_reviews_array);
                                ovrl_rating = review_obj.getString("rating");
                                Log.e("over all rating..", "over all rating.." + ovrl_rating);

                            }


                            rating_percent = review_obj.getString("rating");
                            rating_cal = Float.parseFloat(rating_percent);
                            Log.e("rating cal..", "rating cal.." + rating_cal);

                        }
                    }
                } else {
                    Log.d("response_null", "null");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            // super.onPostExecute(aVoid);
            // pDialog.cancel();
            pBar.setVisibility(View.INVISIBLE);
            Log.d("inside", "post");
            Picasso.with(ProductDetails.this).load(p_img).into(im_pImage);


            Gallery gallery = (Gallery) findViewById(R.id.gallery1);
            gallery.setAdapter(new ImageAdapter(getApplicationContext()));
            Picasso.with(getApplicationContext()).load(images_array.get(0))
                    .fit().into(im_pImage);
            gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    Picasso.with(getApplicationContext()).load(images_array.get(position))
                            .fit().into(im_pImage);
                }
            });


            app_title.setText(p_name);
            tv_pName.setText(p_name);
            Log.e("special price..", "detail sp price..." + special_price);
            if (special_price.equals("0.00")) {
                tv_product_price_single_txt.setVisibility(View.VISIBLE);
                tv_product_price_single_txt.setText("Rs." + p_price);
                // tv_pPrice_strikeout.setText("Rs." + p_price);
                tv_product_price_single_txt.setTextAppearance(getApplicationContext(), R.style.product_price_txt);
                tv_pPrice_strikeout.setVisibility(View.INVISIBLE);

                tv_pPrice.setVisibility(View.INVISIBLE);


               /* LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
                llp.setMargins(50, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
                tv_pPrice_strikeout.setLayoutParams(llp);*/

            } else {
                tv_pPrice_strikeout.setPaintFlags(tv_pPrice_strikeout.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                tv_pPrice_strikeout.setText("Rs." + p_price);
                tv_pPrice.setText("Rs." + special_price);
            }

            tv_pDescriptn.setText(p_description);

            availability = Integer.parseInt(availability_value);
            user_total_reviews_txt.setText(review_length + "\t" + "User Reviews");

            if (review_length == 0) {
                user_total_reviews_txt.setText("Be the first to write review");

            } else {
                user_total_reviews_txt.setText(review_length + "\t" + "User Reviews");

                LayerDrawable stars = (LayerDrawable) ratingBar_userReviews.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
                rating_value = ((rating_cal / 100) * 5);
                Log.e("rating value..", "rating value.." + rating_value);
                final_rating = (float) Math.round(rating_value * 100) / 100;
                ratingBar_userReviews.setRating(final_rating);
                Log.e("final rating..", "final rating.." + final_rating);

            }


            reviews_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (review_length == 0) {

                        review_dialog = new Dialog(ProductDetails.this, R.style.AppTheme);
                        review_dialog.setCancelable(false);
                        review_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        review_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        review_dialog.setContentView(R.layout.custom);

                        submit_review_btn = (Button) review_dialog.findViewById(R.id.submit_review_btn);
                        submit_user_review_ratebar = (RatingBar) review_dialog.findViewById(R.id.review_submit_ratebar);
                        review_submit_editText = (EditText) review_dialog.findViewById(R.id.review_sub_edittxt);
                        submit_mandratory_txt = (TextView) review_dialog.findViewById(R.id.sumit_mandratory_msg);
                        submit_rating_alert = (TextView) review_dialog.findViewById(R.id.submit_rating_alert);
                        close_review_dialog = (ImageView) review_dialog.findViewById(R.id.review_submit_close);


                        submit_user_review_ratebar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                            @Override
                            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                                // Log.e("rating value...", "rating start are..." + String.valueOf(rating));
                                Drawable drawable = ratingBar.getProgressDrawable();
                                drawable.setColorFilter(Color.parseColor("#CF0A8B"), PorterDuff.Mode.SRC_ATOP);


                            }
                        });


                        close_review_dialog.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                // TODO Auto-generated method stub
                                review_dialog.dismiss();
                            }
                        });


                        submit_review_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                submitted_user_review_string = review_submit_editText.getText().toString();
                                if (submit_user_review_ratebar.getRating() == 0 && submitted_user_review_string.isEmpty() && submitted_user_review_string.equals("")) {
                                    submit_rating_alert.setVisibility(View.VISIBLE);
                                    submit_mandratory_txt.setVisibility(View.VISIBLE);
                                    // Log.e("condition 1..", "condition 1.." + user_review);

                                } else if (submit_user_review_ratebar.getRating() == 0) {
                                    submit_rating_alert.setVisibility(View.VISIBLE);
                                    submit_mandratory_txt.setVisibility(View.INVISIBLE);
                                    // Log.e("condition 2..", "condition 2.." + user_review);

                                } else if (submitted_user_review_string.isEmpty() || submitted_user_review_string.equals("")) {
                                    // Log.e("above conditions sucess", "above conditions sucess.." + "\t" + "user reviews..." + user_review);

                                    submit_rating_alert.setVisibility(View.INVISIBLE);
                                    submit_mandratory_txt.setVisibility(View.VISIBLE);
                                } else {
                                    submit_rating_alert.setVisibility(View.INVISIBLE);
                                    submit_mandratory_txt.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "Review Sumitted", Toast.LENGTH_SHORT).show();
                                    review_submit_editText.setText("");
                                    submit_user_review_ratebar.setRating(0F);
                                    review_dialog.dismiss();
                                }


                            }
                        });


                        review_dialog.show();


                    } else {
                        Log.e("Already reviews ..", "already reviews are there..");
                        Intent intent = new Intent(getApplicationContext(), Reviews.class);


                        // intent.putExtra("review_array",user_reviews_array);
                        intent.putStringArrayListExtra("review_array", user_reviews_array);
                        intent.putStringArrayListExtra("review_array_rating", SingletonActivity.individual_rating_array);
                        startActivity(intent);


                    }
                }
            });


            im_pImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ProductDetailsImage.class);
                    intent.putStringArrayListExtra("images_array", images_array);
                    intent.putExtra("product_name", p_name);
                    startActivity(intent);
                }
            });
            if (availability >= 1) {
                stock_txt.setText("In Stock");
                stock_txt.setTextColor(Color.parseColor("#008000"));
            } else {
                stock_txt.setText("Out of Stock");
                stock_txt.setTextColor(Color.parseColor("#FF0000"));
            }
            if (flvr_title_array.size() == 0) {
                flvr_outer.setVisibility(View.GONE);
            }
            if (wt_title_array.size() < 1) {
                wt_outer.setVisibility(View.GONE);
            }

            if (wt_title_array != null) {
                ArrayAdapter<String> wt_Adapter = new ArrayAdapter<String>(ProductDetails.this, android.R.layout.simple_spinner_item, wt_title_array);
                wt_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                wt_spinner.setAdapter(wt_Adapter);

                wt_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            if (flvr_title_array != null) {
                ArrayAdapter<String> flvr_Adapter = new ArrayAdapter<String>(ProductDetails.this, android.R.layout.simple_spinner_item, flvr_title_array);
                flvr_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                flvr_spinner.setAdapter(flvr_Adapter);

                flvr_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            salesPkg_des.setText(sales_packages_value);
            Log.e("banaglore price.", "bangalore price.." + bangalore_price);

            if (bangalore_price.contains("0") || bangalore_price.equals("null")) {
                p_det_priceTxt_1.setVisibility(View.GONE);
                p_det_price_1.setVisibility(View.GONE);

            } else {
                p_det_priceTxt_1.setVisibility(View.VISIBLE);
                p_det_price_1.setVisibility(View.VISIBLE);
                p_det_price_1.setText("Rs." + bangalore_price.replaceAll(".0000", ".00"));
            }
            if (mumbai_price.equals("null") || mumbai_price.contains("0")) {
                p_det_priceTxt_3.setVisibility(View.GONE);
                p_det_price_3.setVisibility(View.GONE);

            } else {
                p_det_priceTxt_3.setVisibility(View.VISIBLE);
                p_det_price_3.setVisibility(View.VISIBLE);
                p_det_price_3.setText("Rs." + mumbai_price.replaceAll(".0000", ".00"));
            }
            if (delhi_price.contains("0") || delhi_price.equals("null")) {
                p_det_priceTxt_2.setVisibility(View.GONE);
                p_det_price_2.setVisibility(View.GONE);


            } else {
                p_det_priceTxt_2.setVisibility(View.VISIBLE);
                p_det_price_2.setVisibility(View.VISIBLE);
                p_det_price_2.setText("Rs." + delhi_price.replaceAll(".0000", ".00"));
            }
            if (bangalore_price.equals("null") && mumbai_price.equals("null") && mumbai_price.contains("null") || bangalore_price.contains("0") && mumbai_price.contains("0") && mumbai_price.contains("0")) {
                table_det_pg_price.setVisibility(View.GONE);
            }

            table_additonl.removeAllViews();
            for (int i = 0; i < attrib_array.size(); i++) {
                Log.e("attrib array size..", "attrib array size.." + attrib_array.size());
                TableRow tr = new TableRow(ProductDetails.this);
                TableRow.LayoutParams TRparam = new TableRow.LayoutParams(
                        TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT);
                TRparam.setMargins(2, 2, 2, 2);

                tr.setLayoutParams(TRparam);

                TextView tv1 = new TextView(ProductDetails.this);
                tv1.setLayoutParams(new TableRow.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tv1.setText(attrib_array.get(i).get("label"));

                tv1.setTypeface(null, Typeface.BOLD);
                tv1.setPadding(7, 5, 3, 5);
                tv1.setBackgroundColor(Color.WHITE);
                tv1.setTypeface(face);

                tr.addView(tv1);

                TextView tv2 = new TextView(ProductDetails.this);
                tv2.setLayoutParams(new TableRow.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tv2.setText(attrib_array.get(i).get("value"));
                Log.e("values ...", "values ...." + "\t" + attrib_array.get(i).get("label") + "\t" + attrib_array.get(i).get("value"));
                // tv2.setTypeface(null, Typeface.BOLD);
                tv2.setBackgroundColor(Color.WHITE);
                tv2.setPadding(3, 5, 3, 5);
                tv2.setTypeface(face);
                tr.addView(tv2);
                table_additonl.addView(tr, new TableLayout.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT));

            }


        }
    }


    public class ImageAdapter extends BaseAdapter {
        private Context context;
        private int itemBackground;

        public ImageAdapter(Context c) {
            context = c;
            // sets a grey background; wraps around the images
            TypedArray a = obtainStyledAttributes(R.styleable.MyGallery);
            itemBackground = a.getResourceId(R.styleable.MyGallery_android_galleryItemBackground, 1);
            a.recycle();
        }

        // returns the number of images
        public int getCount() {
            return images_array.size();
        }

        // returns the ID of an item
        public Object getItem(int position) {
            return position;
        }

        // returns the ID of an item
        public long getItemId(int position) {
            return position;
        }

        // returns an ImageView view
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(context);
            Picasso.with(getApplicationContext()).load(images_array.get(position))
                    .fit().into(imageView);
            imageView.setLayoutParams(new Gallery.LayoutParams(140, 140));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setBackgroundResource(itemBackground);
            return imageView;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.custom_toolbar_menu, menu);
        return true;
    }
}
