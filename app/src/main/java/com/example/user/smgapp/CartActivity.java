package com.example.user.smgapp;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CartActivity extends NavigationDrawer {

    TableLayout tableclick,table_list;
    Button proceedtocheckout,clearallitemsbtn;
    RelativeLayout cartrelative,viewbelow,tablerelative;
    TextView clickheretxt,app_title, totalcartsubtotaltxt,totalcartgrandtotaltxt ;
    EditText tv_pincode;
    TextView delvry_validatn_txt,pincode_validatn_txt,cartcounttxt;
    ProgressBar pBar;

    AutoCompleteTextView delivryCity;
    ArrayList<HashMap<String, String>> getPinCodeandCityList;
    DBController controller = new DBController(this);
    int maxlen=6;
    int autocompletelength=2;
    ArrayList<String> citieslist = new ArrayList<String>();
    String autocompletetext;

    public static final String MyPREFERENCES = "MyPrefs";
    static SharedPreferences prefs;
    String custidstr,custnamestr, custemailstr;

    ArrayList<String> idcartproductdetails = new ArrayList<String>();
    ArrayList<String> namecartproductdetails = new ArrayList<String>();
     ArrayList<String> imgcartproductdetails = new ArrayList<String>();
   ArrayList<String> pricecartproductdetails = new ArrayList<String>();
    ArrayList<String> buyatcartproductdetails = new ArrayList<String>();
    ArrayList<String> bookdesccartproductdetails = new ArrayList<String>();
    ArrayList<String> imgarraycartproductdetails = new ArrayList<String>();
    ArrayList<String> quantitydetails = new ArrayList<String>();


    ArrayList<Float> totalvalueofallcart = new ArrayList<Float>();
    ArrayList<Float>  totalvalueofallcartafteredit = new ArrayList<Float>();

    static String totalbuystr;

    static int totalcount;
    static int  quantityintval, editqtyval;
    float subtotalvalfloatnoneditqty,subtotalvalfloateditqty;

    String pincodestr ;
    String cityfromdb ;
    String subtotalvalfloattostr;
   // Button updatecartbtn;

    String updatecart_url,productidstr;
  public static String quantitystr;
    float subtotalvalfloat;
    String subtotalvalstr,pricevalstr;
    ProgressDialog pdia;

    float finaltotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.cart_activity_main);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.cart_activity_main, null, false);
        drawer.addView(contentView, 0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");



        idcartproductdetails.clear();
        namecartproductdetails.clear();
       imgcartproductdetails.clear();
      pricecartproductdetails.clear();
       buyatcartproductdetails.clear();
      bookdesccartproductdetails.clear();
       imgarraycartproductdetails.clear();
        citieslist.clear();
        quantitydetails.clear();

        app_title = (TextView) findViewById(R.id.app_title);
        app_title.setText("Cart");

        cartcounttxt = (TextView)findViewById(R.id.cartnumber);
        delvry_validatn_txt=(TextView)findViewById(R.id.delvry_validatn_txt);
        pincode_validatn_txt=(TextView)findViewById(R.id.pincode_validtn_txt);
        pBar = (ProgressBar) findViewById(R.id.progress_bar_custom_cart);

        totalcartsubtotaltxt = (TextView)findViewById(R.id.subtotalvaluetext);
        totalcartgrandtotaltxt = (TextView)findViewById(R.id.grandtotalvaluetext);


       /* clearallbtn=(Button)findViewById(R.id.clearallitembutton);

        clearallbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteAllVolleyApi();

            }
        });*/

        System.out.println("PRODUCT ID from singleton IN CartActivity IS----" + SingletonActivity.productid);

        System.out.println("CUST ID from singleton IN CartActivity IS----" + SingletonActivity.custidstr);
        System.out.println("CUST NAME IN CartActivity IS----" + SingletonActivity.custnamestr);
        System.out.println("CUST EMAIL IN CartActivity IS----" + SingletonActivity.custemailstr);

        //updatecart_url="http://localhost:8080/smg_dev/api/update_cart.php?product_id=591&product_qty=10&customer_id=89";

        updatecart_url=SingletonActivity.API_URL+"api/update_cart.php?product_id="+SingletonActivity.productid+"&product_qty="+10+"&customer_id="+SingletonActivity.custidstr;


        citieslist = SingletonActivity.citiesarray;
        delivryCity=(AutoCompleteTextView)findViewById(R.id.delivryCity);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, citieslist);
        delivryCity.setAdapter(adapter);

        delivryCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                autocompletetext = (String) parent.getItemAtPosition(position);
          //      delivryCity.setEnabled(false);
                //TODO Do something with the selected text
            }
        });


        tv_pincode=(EditText)findViewById(R.id.tv_det_pincode);
        tv_pincode.setEnabled(false);

        delivryCity.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (delivryCity.getText().toString().length() >= autocompletelength) {

                    tv_pincode.setEnabled(true);

                }
                return false;
            }
        });

        tv_pincode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (tv_pincode.getText().toString().length() >= maxlen) {
                    pincodestr = tv_pincode.getText().toString();
                    cityfromdb = controller.getSingleCityEntry(pincodestr);

                    if (autocompletetext.equals(cityfromdb)) {
                        Toast.makeText(CartActivity.this, "this city is serviceable", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CartActivity.this, "this city is not serviceable", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

       /* prefs = CartActivity.this.getSharedPreferences(MyPREFERENCES, 0);
        //1)key=Name and 2)key=Emailid 3)key=Phonenos
        custidstr = prefs.getString("CustId", "");
        custnamestr = prefs.getString("CustName", "");
        custemailstr = prefs.getString("CustEmail", "");

        System.out.println("CUST ID IN CartActivity IS----" + custidstr);
        System.out.println("CUST NAME IN CartActivity IS----" + custnamestr);
        System.out.println("CUST EMAIL IN CartActivity IS----" + custemailstr);*/



        tablerelative = (RelativeLayout)findViewById(R.id.RelativeLayout1);
        clickheretxt = (TextView)findViewById(R.id.heretext);
        table_list = (TableLayout)findViewById(R.id.table_main);

        cartrelative = (RelativeLayout)findViewById(R.id.cartrelativehead);
        cartrelative.setVisibility(View.INVISIBLE);
        tableclick = (TableLayout)findViewById(R.id.tablehere);
        tableclick.setVisibility(View.INVISIBLE);
     //   updatecartbtn = (Button)findViewById(R.id.updatecartbutton);
        clearallitemsbtn = (Button)findViewById(R.id.clearallitembutton);
        viewbelow = (RelativeLayout)findViewById(R.id.belowview);
        viewbelow.setVisibility(View.INVISIBLE);


     /*   updatecartbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editsinglecartquantity();

            }
        });
*/
        clearallitemsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cartrelative.removeAllViews();

                DeleteAllVolleyApi();

            }
        });

        clickheretxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CartActivity.this, HomePage.class);
                startActivity(intent);

            }
        });





        proceedtocheckout = (Button) findViewById(R.id.proceedtocheckoutbutton);
        proceedtocheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 if(delivryCity.getText().toString().isEmpty()||delivryCity.getText().toString().equals(""))
                {
                    // Toast.makeText(ProductDetails.this,"Choose delivery city..!",Toast.LENGTH_SHORT).show();
                    delivryCity.requestFocus();
                    delvry_validatn_txt.setVisibility(View.VISIBLE);
                    delvry_validatn_txt.setText("Choose delivery city..!");



                }else if (tv_pincode.getText().toString().equals("") && (!delivryCity.getText().toString().equals(""))){
                     tv_pincode.setEnabled(true);
                     pincode_validatn_txt.setVisibility(View.VISIBLE);
                     delvry_validatn_txt.setText("");

                     pincode_validatn_txt.setText("Enter proper pincode based on city");
                 }
                else {


                    if (autocompletetext.equals(cityfromdb)) {
                        //Toast.makeText(CartActivity.this, "this city is serviceable", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(CartActivity.this, "wrong pincode", Toast.LENGTH_SHORT).show();
                    }

                 }
            }
        });

        TotalCartVolleyApi();



    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    private void editsinglecartquantity(String quantity,String prodid) {


      pdia = new ProgressDialog(CartActivity.this);
      pdia.setMessage("Please Wait...");
      pdia.setCanceledOnTouchOutside(false);
      pdia.show();

        System.out.println("url for edit single csrt updating quantity IS" + SingletonActivity.API_URL + "api/update_cart.php?product_id=" + prodid + "&product_qty=" + quantity + "&customer_id=" + SingletonActivity.custidstr);

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                SingletonActivity.API_URL+"api/update_cart.php?product_id="+prodid+"&product_qty="+quantity+"&customer_id="+SingletonActivity.custidstr, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {



                        System.out.println("RESPONSE IN CARTACTIVITY for EDIT SINGLE CART QUANTITY IS" + response);
                        //Log.d("Request", response.toString());

                        try {

                            //   JSONObject jsonObject = new JSONObject(response);

                            String isSuccessStr = response.getString("isSuccess");

                           // String messageStr = jsonObject.getString("message");

                            System.out.println("IS SUCCESS STRING IN EDIT SINGLE CART  IS" + isSuccessStr);
                            //System.out.println("MESSAGE STRING IN DELETE CART VOLLEY ACTIVITY IS" + messageStr);

                            if(isSuccessStr.equalsIgnoreCase("true")){

                                 pdia.dismiss();
                                Toast.makeText(getApplicationContext(),"Successfully your cart is updated",Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            System.out.println("Exception" + e);
                        }





                    }
                },
                new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pdia.dismiss();
                VolleyLog.d("Request", "Error: " + error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
         /*   params.put("phone", phone);
            params.put("name", username);
            params.put("pwd",password);
            params.put("email", email);*/
                return params;
            }
        };

        GlobalClass.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void DeleteAllVolleyApi(){


        StringRequest stringRequest = new StringRequest(Request.Method.POST,SingletonActivity.API_URL+"api/delete_cart_items.php?customerId="+ SingletonActivity.custidstr ,

                //    StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://52.77.145.35/api/totalcart.php?customer_id=112",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // pdia.dismiss();

                        System.out.println("RESPONSE IN DELETE CART VOLLEY ACTIVITY IS" + response);

                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            String isSuccessStr = jsonObject.getString("isSuccess");

                            String messageStr = jsonObject.getString("message");

                            System.out.println("IS SUCCESS STRING IN DELETE CART VOLLEY ACTIVITY IS" + isSuccessStr);
                            System.out.println("MESSAGE STRING IN DELETE CART VOLLEY ACTIVITY IS" + messageStr);

                            if(isSuccessStr.equalsIgnoreCase("true")){
                                cartrelative.removeAllViews();
                                viewbelow.setVisibility(View.VISIBLE);
                                tableclick.setVisibility(View.VISIBLE);
                                cartcounttxt.setText("0");
                            }




                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            System.out.println("Exception" + e);
                        }






                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CartActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        //  pdia.dismiss();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                 params.put("customer_id",SingletonActivity.custidstr);

                return params;
            }

        };

        GlobalClass.getInstance().addToRequestQueue(stringRequest);
        //RequestQueue requestQueue = Volley.newRequestQueue(CartActivity.this);
       // requestQueue.add(stringRequest);
    }


    private void TotalCartVolleyApi(){

      /*pdia = new ProgressDialog(SignupActivityVolley.this);
      pdia.setMessage("Please Wait...");
      pdia.setCanceledOnTouchOutside(false);
      pdia.show();*/
         pBar.setVisibility(View.VISIBLE);

      //  SingletonActivity.custidstr="112";
        StringRequest stringRequest = new StringRequest(Request.Method.POST,SingletonActivity.API_URL+"api/totalcart.php?customer_id="+ SingletonActivity.custidstr ,

            //    StringRequest stringRequest = new StringRequest(Request.Method.POST,"http://52.77.145.35/api/totalcart.php?customer_id=112",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // pdia.dismiss();
                        pBar.setVisibility(View.INVISIBLE);

                        System.out.println("RESPONSE IN TOTAL CART VOLLEY ACTIVITY IS" + response);

                        try {
                            JSONObject mainObject = new JSONObject(response);

                            System.out.println("JSON OBJECT RESPONSE IS---->" + mainObject);

                            String issuccessstr = mainObject.getString("isSuccess");
                             totalbuystr = mainObject.getString("total_buy");

                            System.out.println("TOTAL BUY IS " + totalbuystr);

                            SingletonActivity.cartcount = totalbuystr;

                         SharedPreferences.Editor editor = getSharedPreferences(
                                    MyPREFERENCES, MODE_PRIVATE).edit();
                            editor.putString("cartcount", SingletonActivity.cartcount);

                            editor.commit();

                            cartcounttxt.setText(SingletonActivity.cartcount);

                            JSONArray resultjsonArray = mainObject.getJSONArray("result");

                            System.out.println("JSON ARRAY IN TOTAL CART VOLLEY ACTIVITY IS---" + resultjsonArray);

                            for(int i = 0; i < resultjsonArray.length(); i++) {
                                JSONObject resultjsonobject = resultjsonArray.getJSONObject(i);

                                System.out.println("JSON OBJECT IN TOTAL CART VOLLEY ACTIVITY IS---" + resultjsonobject);

                                String idstr = resultjsonobject.getString("id");
                                String skustr = resultjsonobject.getString("sku");
                                String namestr = resultjsonobject.getString("name");
                                String imgurlstr = resultjsonobject.getString("img_url");
                                String pricestr = resultjsonobject.getString("price");
                                String buyatstr = resultjsonobject.getString("buy_at");
                                String bookdescstr = resultjsonobject.getString("book_description");
                                String imgarraystr = resultjsonobject.getString("image_array");
                                String qtystr = resultjsonobject.getString("quantity");

                                System.out.println("ID STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + idstr);
                                System.out.println("SKU STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + skustr);
                                System.out.println("NAME STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + namestr);
                                System.out.println("IMG URL STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + imgurlstr);
                                System.out.println("PRICE STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + pricestr);
                                System.out.println("BUY AT STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + buyatstr);
                                System.out.println("BOOK DESCRIPTION STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + bookdescstr);
                                System.out.println("IMG ARRAY STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + imgarraystr);
                                System.out.println("QUANTITY STRING IN TOTAL CART VOLLEY ACTIVITY IS---" + qtystr);

                                idcartproductdetails.add(resultjsonobject.getString("id"));
                                namecartproductdetails.add(resultjsonobject.getString("name"));
                                imgcartproductdetails.add(resultjsonobject.getString("img_url"));
                                pricecartproductdetails.add(resultjsonobject.getString("price"));
                                buyatcartproductdetails.add(resultjsonobject.getString("buy_at"));
                                bookdesccartproductdetails.add(resultjsonobject.getString("book_description"));
                                imgarraycartproductdetails.add(resultjsonobject.getString("image_array"));
                                quantitydetails.add(resultjsonobject.getString("quantity"));

                            }

                            init();

                            System.out.println("IS SUCCESS STRING IN TOTAL CART VOLLEY ACTIVITY IS---"+ issuccessstr);
                            System.out.println("TOTAL BUY STRING IN TOTAL CART VOLLEY ACTIVITY IS---"+ totalbuystr);

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            System.out.println("Exception-------" + e);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CartActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        pBar.setVisibility(View.INVISIBLE);
                        //  pdia.dismiss();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();


                  params.put("customer_id",SingletonActivity.custidstr);


                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(CartActivity.this);
        requestQueue.add(stringRequest);
    }





    public void init() {

        for (int j = 0; j < namecartproductdetails.size(); j++) {

            totalcount  = Integer.parseInt(totalbuystr);

            System.out.println("TOTAL COUNT DETAILS---" + totalcount);

            System.out.println("NAME CART PRODUCT DETAILS---" + namecartproductdetails.size());


        final TableLayout stk = (TableLayout) findViewById(R.id.table_main);
            // TableRow tbrow = new TableRow(this);
            LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            final RelativeLayout row = (RelativeLayout) inflater.inflate(R.layout.cart_list_row, null);



            final  TextView priceval = (TextView) row.findViewById(R.id.priceval);
           final TextView subtotalval = (TextView) row.findViewById(R.id.subtotalval);
            final TextView prodid = (TextView) row.findViewById(R.id.pinnumbertext);
            ImageView cartimg = (ImageView) row.findViewById(R.id.imageView);
            Button updatecartbtninrow = (Button) row.findViewById(R.id.updatecartbutton);

            TextView textView = (TextView) row.findViewById(R.id.imagenametext);
            final  EditText quantityedittext = (EditText)row.findViewById(R.id.editText);

            quantityedittext.addTextChangedListener(new TextWatcher(){
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                    if (quantityedittext.getText().toString().matches("^0") )
                    {
                        // Not allowed
                        Toast.makeText(CartActivity.this, "not allowed", Toast.LENGTH_LONG).show();
                        quantityedittext.setText("");
                    }
                }
                @Override
                public void afterTextChanged(Editable arg0) { }
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            });

            pricevalstr = buyatcartproductdetails.get(j);
            priceval.setText(buyatcartproductdetails.get(j));

            productidstr = idcartproductdetails.get(j);
            prodid.setText(idcartproductdetails.get(j));

      //     editsinglecartquantity(quantityedittext.getText().toString(), productidstr);




            if(buyatcartproductdetails.get(j).equalsIgnoreCase("0.00")){

                pricevalstr = pricecartproductdetails.get(j);
                priceval.setText(pricecartproductdetails.get(j));
            }


            quantityedittext.setText(quantitydetails.get(j));
            quantityintval = Integer.parseInt(quantityedittext.getText().toString());
            System.out.println("EDITTEXT QUANTITY IS" + quantityintval);


            //--------------------------------------
            quantitystr = quantityedittext.getText().toString();


            System.out.println("QUANTITY IN EDITTEXT ARE--------" + quantitystr);

            editqtyval = Integer.parseInt(quantityedittext.getText().toString());
            System.out.println("EDITTEXT UPDATED QUANTITY IS " + editqtyval);

            System.out.println("PRODUCT ID IN EDIT QTY ONCLICK ARE--------" + prodid.getText().toString());






            float pricevalfloat = Float.parseFloat(priceval.getText().toString());
            System.out.println("PRICE IN FLOAT NOT IN ONCLICK" + pricevalfloat);

            subtotalvalfloat = pricevalfloat *  editqtyval;
            System.out.println("SUBTOTAL VALUE NOT IN ONCLICK " + subtotalvalfloat);

            subtotalvalfloattostr = Float.toString(subtotalvalfloat);
            System.out.println("SUBTOTAL VALUE float to string is not in onclick" + subtotalvalfloattostr);
            subtotalval.setText(subtotalvalfloattostr);

          /*  subtotalvalfloatnoneditqty = Float.parseFloat(subtotalval.getText().toString());

            if(editqtyval>0) {
                finaltotal  = subtotalvalfloat - (subtotalvalfloatnoneditqty / editqtyval);
            }
            else{
                finaltotal = subtotalvalfloat - pricevalfloat;
            }*/
            //   totalvalueofallcart.clear();

            totalvalueofallcart.add(subtotalvalfloat);
            System.out.println("TOTAL VALUE OF ALL CART is not in onclick" +  totalvalueofallcart);

            float sumoutside = 0;
            for(int i=0; i<totalvalueofallcart.size(); i++){
                sumoutside += totalvalueofallcart.get(i);

            }

            System.out.println("Grand total is not in onclick" + sumoutside);
            String sumstr = Float.toString(sumoutside);
            totalcartsubtotaltxt.setText(sumstr);
            totalcartgrandtotaltxt.setText(sumstr);

            //--------------------------------

          //  if ( quantityintval == 1)
           // {
       /*         subtotalval.setText(pricevalstr);
                subtotalvalfloatnoneditqty = Float.parseFloat(subtotalval.getText().toString());
                totalvalueofallcart.add(subtotalvalfloatnoneditqty);
                System.out.println("TOTAL VALUE OF ALL CART" +  totalvalueofallcart);

               float sum = 0;
                for(int i=0; i<totalvalueofallcart.size(); i++){
                    sum += totalvalueofallcart.get(i);

                }

                System.out.println("Grand total is" + sum);
                String sumstrout = Float.toString(sum);
                totalcartsubtotaltxt.setText(sumstrout);
                totalcartgrandtotaltxt.setText(sumstrout);
*/

        //    }

            updatecartbtninrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   // totalvalueofallcart.clear();

                    quantitystr = quantityedittext.getText().toString();
                  //  editsinglecartquantity(quantitystr);

                    System.out.println("QUANTITY IN EDITTEXT ARE--------" + quantitystr);

                    editqtyval = Integer.parseInt(quantityedittext.getText().toString());
                    System.out.println("EDITTEXT UPDATED QUANTITY IS " + editqtyval);

                    System.out.println("PRODUCT ID IN EDIT QTY ONCLICK ARE--------" + prodid.getText().toString());




                   editsinglecartquantity(quantityedittext.getText().toString(), prodid.getText().toString());

                    float pricevalfloat = Float.parseFloat(priceval.getText().toString());
                    System.out.println("PRICE IN FLOAT " + pricevalfloat);

                    subtotalvalfloat = pricevalfloat *  editqtyval;
                    System.out.println("SUBTOTAL VALUE " + subtotalvalfloat);

                    subtotalvalfloattostr = Float.toString(subtotalvalfloat);
                    System.out.println("SUBTOTAL VALUE float to string in onclick" + subtotalvalfloattostr);
                    subtotalval.setText(subtotalvalfloattostr);

                    subtotalvalfloatnoneditqty = Float.parseFloat(subtotalval.getText().toString());

                    if(editqtyval>0) {
                        finaltotal  = subtotalvalfloat - (subtotalvalfloatnoneditqty / editqtyval);
                    }
                    else{
                        finaltotal = subtotalvalfloat - pricevalfloat;
                    }
                 //   totalvalueofallcart.clear();

                    totalvalueofallcart.add(finaltotal);
                    System.out.println("TOTAL VALUE OF ALL CART in onclick" +  totalvalueofallcart);

                    float sum = 0;
                    for(int i=0; i<totalvalueofallcart.size(); i++){
                        sum += totalvalueofallcart.get(i);

                    }

                    System.out.println("Grand total is in onclick" + sum);
                    String sumstr = Float.toString(sum);
                    totalcartsubtotaltxt.setText(sumstr);
                    totalcartgrandtotaltxt.setText(sumstr);

                }
            });

            System.out.println("NAME CART PRODUCT ARE--------" + namecartproductdetails.get(j));
            textView.setText(namecartproductdetails.get(j));

            System.out.println("PRICE VALUE IN INIT--------" + pricecartproductdetails.get(j));
            System.out.println("BUY AT CART VALUE IN INIT--------" + buyatcartproductdetails.get(j));



            Picasso.with(CartActivity.this).load(imgcartproductdetails.get(j)).placeholder(R.drawable.loading)
                    .fit().into(cartimg);

            //     textView.setText(namecartproductdetails.get(2));
            ImageView closeimg = (ImageView) row.findViewById(R.id.deleterow);

            quantityedittext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (v.getId() == quantityedittext.getId()) {
                        quantityedittext.setCursorVisible(true);
                    }
                }
            });

            quantityedittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        hideKeyboard();
                    }
                }

                private void hideKeyboard() {
                    if (quantityedittext != null) {
                        InputMethodManager imanager = (InputMethodManager) CartActivity.this
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        imanager.hideSoftInputFromWindow(quantityedittext.getWindowToken(), 0);

                    }

                }
            });


         //   textView.setText("earthh");


       stk.addView(row,j);
            cartrelative.setVisibility(View.VISIBLE);

      //    stk.addView(row, i);
            closeimg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    stk.removeView(row);

                }
            });

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_toolbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);




        SearchManager searchManager = (SearchManager) CartActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(CartActivity.this.getComponentName()));
            handleIntent(getIntent());
        }
        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("query....", "query...." + query);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Typeface face = Typeface.createFromAsset(getApplication().getAssets(),"fonts/OpenSans-Regular.ttf");    //  THIS



        if (id == R.id.action_login) {

            Intent intent = new Intent(CartActivity.this, Login.class);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.action_terms) {
            Intent in = new Intent(this, TermsandCondition.class);
            startActivity(in);
        }


        return super.onOptionsItemSelected(item);


    }


}







