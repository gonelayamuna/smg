package com.example.user.smgapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user on 13-06-2016.
 */
public class Reviews extends NavigationDrawer {
    TextView app_title;
    ArrayList<String> reviews_array;
    // TextView username, user_review, user_review_date, review_title;
    ListView review_list;
    ArrayList<HashMap<String,String>> final_reviews_array;
    ReviewAdapter adapter;
    HashMap<String,String> reviews_map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.reviews, null, false);
        drawer.addView(contentView, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        app_title = (TextView) findViewById(R.id.app_title);
        app_title.setText("User Reviews");

        review_list = (ListView) findViewById(R.id.reviews_list);




        final_reviews_array = new ArrayList<>();

        SingletonActivity.individual_rating_array = new ArrayList<>();
        SingletonActivity.rating_title_array = new ArrayList<>();
        SingletonActivity.user_review_array = new ArrayList<>();
        SingletonActivity.rating_name_array = new ArrayList<>();

        for (int i = 0; i < SingletonActivity.individual_rating_array.size(); i++) {
            reviews_map = new HashMap<>();

           // username.setText(SingletonActivity.rating_name_array.get(i));
            Log.e("user name....", "user name..." + SingletonActivity.rating_name_array.get(i));
           // review_title.setText(SingletonActivity.rating_title_array.get(i));
          //  user_review.setText(SingletonActivity.user_review_array.get(i));
            reviews_map.put("username",SingletonActivity.rating_name_array.get(i));
            final_reviews_array.add(reviews_map);


        }


        adapter=new ReviewAdapter(getApplicationContext(),final_reviews_array);
        review_list.setAdapter(adapter);

    }


    public class ReviewAdapter extends BaseAdapter {
        ArrayList<HashMap<String,String>> review_array_adapter;

        Context con;

        public ReviewAdapter(Context context, ArrayList<HashMap<String,String>> review_array_list) {
            this.review_array_adapter = review_array_list;
            this.con = context;
        }


        @Override
        public int getCount() {
            return review_array_adapter.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View list;
            LayoutInflater inflater = (LayoutInflater) con
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                list = new View(con);
                list = inflater.inflate(R.layout.review_row, null);
                TextView username = (TextView) list.findViewById(R.id.review_user_name);
                TextView review_title = (TextView) list.findViewById(R.id.review_title);
                TextView user_review = (TextView) list.findViewById(R.id.review_des);
                TextView user_review_date = (TextView) list.findViewById(R.id.review_date);







            }
            else {
                list = (View) convertView;
            }
            return list;
        }
    }
}
