package com.example.user.smgapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

public class Prd_grid_adapter extends BaseAdapter {


    List<HashMap<String, String>> Category_listData;
    HashMap<String, String> map;
    Context context;
    Typeface face;
    String wishList_url,remove_wishList_url;
    NavigationDrawer nav=new NavigationDrawer();

    private static LayoutInflater inflater=null;
    public Prd_grid_adapter(Activity context, List<HashMap<String, String>> aList) {
        // TODO Auto-generated constructor stub
        Category_listData=aList;
        /*/for(int i=1;i<aList.size();i++)
        {
            Category_listData.add(aList.get(i));
        }*/
        this.context=context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return Category_listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView name,price,original_price;
        ImageView img,wish_list;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        this.face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        rowView = inflater.inflate(R.layout.grid_item_view, null);
        holder.name=(TextView) rowView.findViewById(R.id.p_name);
        holder.img=(ImageView) rowView.findViewById(R.id.p_img);
        holder.wish_list= (ImageView) rowView.findViewById(R.id.wish_list);
        holder.price=(TextView) rowView.findViewById(R.id.p_price);
        holder.original_price= (TextView) rowView.findViewById(R.id.orginal_p);
        map=Category_listData.get(position);
        holder.name.setTypeface(face);
        holder.name.setText(map.get("product_name"));



        holder.wish_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SingletonActivity.custidstr.isEmpty()) {
                    Toast.makeText(context, "Ur not logged in,Please Login", Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Alert!");
                    builder.setMessage("Ur not logged in")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(context, Login.class);
                                    context.startActivity(intent);
                                }


                            });
                    // Create the AlertDialog object and return it
                    builder.show();

                } else {
                    wishList_url = SingletonActivity.API_URL + "api/add_wishlist.php?customer_id=" + SingletonActivity.custidstr + "&product_id=" + Category_listData.get(position).get("product_id");
                    remove_wishList_url = SingletonActivity.API_URL + "api/remove_item_wishlist.php?customerid=" + SingletonActivity.custidstr + "&productid=" + Category_listData.get(position).get("product_id");
                    Log.e("wishlist api..", "wish list api.." + wishList_url);
                    Log.e("remove wishlist api..", "remove wish list api.." + remove_wishList_url);
                    v.setActivated(!v.isActivated());
                    if (v.isActivated()){
                        Toast.makeText(context,"addedd..",Toast.LENGTH_SHORT).show();

                        nav.addTowishList(wishList_url);
                    }
                    else {
                        Toast.makeText(context,"removed....",Toast.LENGTH_SHORT).show();
                        nav.removeFromwishList(remove_wishList_url);
                    }


                }


            }
        });





        if (map.get("special_price").equals("0.00")){
            holder.price.setVisibility(View.INVISIBLE);
            // holder.o_price.setVisibility(View.INVISIBLE);
            holder.original_price.setText("Rs." + map.get("product_price"));
            holder.original_price.setTextAppearance(context, R.style.product_price_txt);

        }
        else{
            holder.price.setText("Rs." + map.get("special_price"));
            holder.original_price.setText("Rs." + map.get("product_price"));
            holder.original_price.setPaintFlags(holder.original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
       /* holder.price.setText("Rs." + map.get("special_price"));
        holder.original_price.setText("Rs."+map.get("product_price"));
        holder.original_price.setPaintFlags(holder.original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);*/
        Picasso.with(context).load(map.get("product_image")).placeholder(R.drawable.loading)
                .fit().into(holder.img);


       /* holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  Log.e("id value for details..", "id value for details.." + Category_listData.get(position).get("product_id"));





            }
        });*/






        return rowView;
    }

}