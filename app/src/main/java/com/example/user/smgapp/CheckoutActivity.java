package com.example.user.smgapp;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckoutActivity extends NavigationDrawer implements TextWatcher {

      String dialogcustnamestr;
    RadioGroup radioGroup;
    ImageView  cancel_dialog_login, cancel_dialog_forgotpwd;
    LinearLayout pwdandconfpwdsenderlinear;
    CheckBox checkBox;
    Button revieworderbtn,editcartbtn,sendgiftbtn,loginbtn,dialogloginbtn,dialogsubmitbtn;
    TextView paymenttxt,forgotpwddialog, backtologinsecdialog,alreadyregistxt;
    Spinner occasion;
    EditText dialogemail,dialogpwd,forgotpwddialogemail;
    EditText fnsenderedittxt,lnsenderedittxt,emailsenderedittxt,pwdedittxt,confirmpwdedittxt;
    EditText fnshippingedittxt,lnshippingedittxt,addressshippingedittxt,cityshippingedittxt,stateshippingedittxt,pincodeshippingedittxt,contactnosshippingedittxt;
    ScrollView scrollView;
    TextInputLayout emailsendertxtinput;
    TextView app_title;


    String[] giftoccasion = {"Select Occasion", "Bangalore", "Hyderabad", "Kerala", "Uttar Pradesh", "Switzerland"};
     TableLayout stk;
    private Dialog mDialog,dialog;
    RelativeLayout checkoutreviewlist;
    static String strdialogmailedittxt, strdialogpwdedittxt, strforgotpwddialogemail;
    static String strfnsenderedittxt,strlnsenderedittxt,  stremailsenderedittxt,strpwdedittxt,strconfirmpwdedittxt;
    static String strfnshipgedittxt,strlnshipedittxt,straddressshipedittxt,strcontactnosshipedittxt;
     String stringcustname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_checkout);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_checkout, null, false);
        drawer.addView(contentView, 0);
        app_title = (TextView) findViewById(R.id.app_title);

        emailsendertxtinput = (TextInputLayout)findViewById(R.id.textinputthree);
        //sender
        fnsenderedittxt = (EditText) findViewById(R.id.firstnameedittext);
        lnsenderedittxt = (EditText) findViewById(R.id.lastnameedittext);

        emailsenderedittxt = (EditText) findViewById(R.id.emailedittext);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        app_title.setText("Checkout");

        revieworderbtn = (Button)findViewById(R.id.revieworderbutton);

        editcartbtn = (Button)findViewById(R.id.editcartbutton);
        occasion = (Spinner) findViewById(R.id.spinner);
        scrollView = (ScrollView) findViewById(R.id.scrollView1);

        alreadyregistxt = (TextView)findViewById(R.id.alreadyregistertext);
        pwdedittxt = (EditText)findViewById(R.id.passwordedittext);
        confirmpwdedittxt = (EditText)findViewById(R.id.confirmpwdedittext);

        checkBox = (CheckBox)findViewById(R.id.checkBox);
        loginbtn = (Button)findViewById(R.id.loginbtn);
        //shipping
        fnshippingedittxt = (EditText) findViewById(R.id.shippingfirstnameedittext);
        lnshippingedittxt = (EditText) findViewById(R.id.shippinglastnameedittext);
        addressshippingedittxt = (EditText) findViewById(R.id.shippingemailedittext);
        contactnosshippingedittxt = (EditText) findViewById(R.id.shippingmobilenosedittext);
        cityshippingedittxt = (EditText) findViewById(R.id.shippingcityedittext);
        stateshippingedittxt = (EditText) findViewById(R.id.shippingstateedittext);
        pincodeshippingedittxt = (EditText) findViewById(R.id.shippingpincodeedittext);


        lnsenderedittxt.addTextChangedListener(this);
        emailsenderedittxt.addTextChangedListener(this);
        pwdedittxt.addTextChangedListener(this);
        confirmpwdedittxt.addTextChangedListener(this);

        fnshippingedittxt.addTextChangedListener(this);
        lnshippingedittxt.addTextChangedListener(this);
        addressshippingedittxt.addTextChangedListener(this);
        contactnosshippingedittxt.addTextChangedListener(this);
        cityshippingedittxt.addTextChangedListener(this);
        stateshippingedittxt.addTextChangedListener(this);
        pincodeshippingedittxt.addTextChangedListener(this);




        System.out.println("CUST ID from singleton IN CheckoutActivity IS----" + SingletonActivity.custidstr);
        System.out.println("CUST NAME from singleton IN CheckoutActivity IS----" + SingletonActivity.custnamestr);
        System.out.println("CUST EMAIL from singleton IN CheckoutActivity IS----" + SingletonActivity.custemailstr);

         stringcustname = SingletonActivity.custnamestr;
     //   SingletonActivity.custnamestr = stringcustname;
        System.out.println("Cust name is in checkoutactivity" +stringcustname);

        if(stringcustname!=null) {
            String[] parts = stringcustname.split(" ");
            String custfirstname = parts[0]; // 004
            String custlastname = parts[1]; // 034556
            System.out.println("PART 1-----" + custfirstname);
            System.out.println("PART 2-----" + custlastname);


                fnsenderedittxt.setText(custfirstname);
                lnsenderedittxt.setText(custlastname);

            emailsendertxtinput.setVisibility(View.GONE);
            checkBox.setVisibility(View.GONE);
            alreadyregistxt.setVisibility(View.GONE);
            loginbtn.setVisibility(View.GONE);
           emailsenderedittxt.setVisibility(View.INVISIBLE);

        }
/*
        else if{
            emailsenderedittxt.setVisibility(View.INVISIBLE);
        }*/



         /*  String str = "Click here to continue shopping";
        String[] parts = str.split(" ");
        String part1 = parts[0]; // 004
        String part2 = parts[1]; // 034556
        System.out.println("PART 2-----"+ part2);
        clickheretxt.setText(str);*/

        fnsenderedittxt.addTextChangedListener(this);

        stk = (TableLayout) findViewById(R.id.table_main);
        init();






        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog = new Dialog(CheckoutActivity.this, R.style.AppTheme);
                mDialog.setCancelable(false);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                mDialog.setContentView(R.layout.login_dialog_box);

                cancel_dialog_login = (ImageView) mDialog.findViewById(R.id.cancel_logindialog);
                dialogemail = (EditText) mDialog.findViewById(R.id.dialogemailedittxt);
                dialogpwd = (EditText) mDialog.findViewById(R.id.dialogpwdedittext);
                forgotpwddialog = (TextView) mDialog.findViewById(R.id.forgotpwd);

                dialogemail.addTextChangedListener(CheckoutActivity.this);
                dialogpwd.addTextChangedListener(CheckoutActivity.this);

                dialogloginbtn = (Button) mDialog.findViewById(R.id.login_btn);

                dialogloginbtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                       strdialogmailedittxt =  dialogemail.getText().toString();
                        strdialogpwdedittxt =  dialogpwd.getText().toString();

                        if ((  strdialogmailedittxt.length()==0)) {
                            dialogemail.setError("Mandatory field");
                        }
                        if (( strdialogpwdedittxt.length()==0)) {
                            dialogpwd.setError("Mandatory field");
                        }

                        else {
                            login();
                        }
/*
                        else if((strdialogmailedittxt.length()>0) && (strdialogpwdedittxt.length()>0)){


                            mDialog.dismiss();
                        }*/

                       // Toast.makeText(getApplicationContext(),"hi hello how r u",Toast.LENGTH_LONG).show();
                    }
                });

                cancel_dialog_login.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        mDialog.dismiss();
                    }
                });

                forgotpwddialog.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        dialog = new Dialog(CheckoutActivity.this, R.style.AppTheme);
                        dialog.setCancelable(false);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        dialog.setContentView(R.layout.forgot_pwd_dialog);

                        cancel_dialog_forgotpwd = (ImageView) dialog.findViewById(R.id.cancel_forgotpwddialog);
                        forgotpwddialogemail = (EditText) dialog.findViewById(R.id.secdiaemailedittxt);
                        dialogsubmitbtn = (Button)dialog.findViewById(R.id.submit_btn);
                        backtologinsecdialog = (TextView) dialog.findViewById(R.id.backtologin);

                        forgotpwddialogemail.addTextChangedListener(CheckoutActivity.this);


                        backtologinsecdialog.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();

                            }
                        });

                        dialogsubmitbtn.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {

                                boolean invalid = false;
                                strforgotpwddialogemail = forgotpwddialogemail.getText().toString();



                                if (!isValidEmail( strforgotpwddialogemail)) {
                                    invalid = true;
                                    forgotpwddialogemail.setError("Mandatory field");
                                }
                                 else{
                                      Toast.makeText(getApplicationContext(), "Check your mail to reset your password and login here.", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }

                              /*  else if (strforgotpwddialogemail.length() > 0){
                                    Toast.makeText(getApplicationContext(), "Check your mail to reset your password.", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();

                                }*/

                            }
                        });


                        cancel_dialog_forgotpwd.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View arg0) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                mDialog.dismiss();
                            }
                        });

                        dialog.show();
                    }
                });

                mDialog.show();

            }
        });

        paymenttxt = (TextView)findViewById(R.id.paymenttext);

        sendgiftbtn = (Button)findViewById(R.id.sendgiftbutton);
        radioGroup = (RadioGroup)findViewById(R.id.radiobuttongroup);
        paymenttxt.setVisibility(View.INVISIBLE);

        pwdandconfpwdsenderlinear = (LinearLayout)findViewById(R.id.senderlineartwo);
        pwdandconfpwdsenderlinear.setVisibility(View.INVISIBLE);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (checkBox.isChecked()) {
                    pwdandconfpwdsenderlinear.setVisibility(View.VISIBLE);
                } else {
                    pwdandconfpwdsenderlinear.setVisibility(View.INVISIBLE);

                }

            }


        });

     radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
         @Override
         public void onCheckedChanged(RadioGroup group, int checkedId) {
             paymenttxt.setVisibility(View.INVISIBLE);
             radioGroup.setSelected(true);
         }
     });



        sendgiftbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strfnsenderedittxt =  fnsenderedittxt.getText().toString();
                strlnsenderedittxt =  lnsenderedittxt.getText().toString();
                stremailsenderedittxt =  emailsenderedittxt.getText().toString();
                strpwdedittxt =  pwdedittxt.getText().toString();
                strconfirmpwdedittxt =  confirmpwdedittxt.getText().toString();


                strfnshipgedittxt =  fnshippingedittxt.getText().toString();
                strlnshipedittxt = lnshippingedittxt.getText().toString();
                straddressshipedittxt =  addressshippingedittxt.getText().toString();
                strcontactnosshipedittxt =  contactnosshippingedittxt.getText().toString();
                strcontactnosshipedittxt =  contactnosshippingedittxt.getText().toString();
                strcontactnosshipedittxt =  contactnosshippingedittxt.getText().toString();

                boolean invalid = false;

             if (!(strfnsenderedittxt.length()>0)) {
                    fnsenderedittxt.setError("Mandatory field");

                }



                if ((  strlnsenderedittxt.length()==0)) {
                    lnsenderedittxt.setError("Mandatory field");
                }

                if (!isValidEmail( stremailsenderedittxt)) {
                    invalid = true;
                    emailsenderedittxt.setError("Mandatory field");
                }
               /* if (( stremailsenderedittxt.length()==0)) {
                    emailsenderedittxt.setError("Mandatory field");
                }*/


                if ((   strpwdedittxt.length()==0)) {
                    pwdedittxt.setError("Mandatory field");
                }

                if (!(strpwdedittxt.equalsIgnoreCase(strconfirmpwdedittxt))){
                    confirmpwdedittxt.setError("Password not match");
                }

               /* if ((   strconfirmpwdedittxt.length()==0)) {
                    confirmpwdedittxt.setError("Mandatory field");
                }*/

                if (( strfnshipgedittxt.length()==0)) {
                    fnshippingedittxt.setError("Mandatory field");
                }

                if (( strlnshipedittxt.length()==0)) {
                    lnshippingedittxt.setError("Mandatory field");
                }

                if (( straddressshipedittxt.length()==0)) {
                    addressshippingedittxt.setError("Mandatory field");
                }

                if ((  strcontactnosshipedittxt.length()==0)) {
                    contactnosshippingedittxt.setError("Mandatory field");
                }

                if(radioGroup.isSelected()){

                        paymenttxt.setVisibility(View.INVISIBLE);


                } else {
                    paymenttxt.setVisibility(View.VISIBLE);

                }


            }
        });

      /*  String cleanPwd1 = Trim.text(edt_password);
        if ((cleanPwd1.length() < 6) || (cleanPwd1.isEmpty())) {
            ShakeAndReport.onShake(edt_password, "Password must be at least six characters long", MainActivity.this);
            return;
        }
*/



        checkoutreviewlist  = (RelativeLayout)findViewById(R.id.reviewlistrelative);
       // stk.setVisibility(View.GONE);
        checkoutreviewlist.setVisibility(View.GONE);


        revieworderbtn.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {

               /* if (stk.getVisibility()==View.VISIBLE) {
                    stk.setVisibility(View.GONE);
                }
             else {
                    stk.setVisibility(View.VISIBLE);
                }*/
             Log.d("hi","hi1");
             Log.d("hi", "hi1");
                 if (checkoutreviewlist.getVisibility()==View.VISIBLE) {
                    checkoutreviewlist.setVisibility(View.GONE);
                     Log.d("hi", "hi2");
                }
             else {
                    checkoutreviewlist.setVisibility(View.VISIBLE);
                     Log.d("hi", "hi3");
                }

         }



     });
        editcartbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });




        //Creating the instance of ArrayAdapter containing list of language names
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (CheckoutActivity.this, android.R.layout.select_dialog_item, giftoccasion);
        occasion.setAdapter(adapter);




    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void init() {

     //  final TableLayout stk = (TableLayout) findViewById(R.id.table_main);
        for (int i = 0; i < 5; i++) {
            // TableRow tbrow = new TableRow(this);

            LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            final RelativeLayout row = (RelativeLayout) inflater.inflate(R.layout.checkout_row, null);


            TextView textView = (TextView) row.findViewById(R.id.imagenametext);
            ImageView checkoutimg = (ImageView) row.findViewById(R.id.imageView);

            textView.setText("checkout");
            //  cartimg.setImageResource(Integer.parseInt(android_image_urls.toString()));

            stk.addView(row, i);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), "HELLO", Toast.LENGTH_LONG).show();
                }
            });


        }

    }



    //For edittext setError Condition using TextWatcher
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (fnsenderedittxt.getText().toString().length()>0) {
            fnsenderedittxt.setError(null);
        }

        if (lnsenderedittxt.getText().toString().length()>0) {
            lnsenderedittxt.setError(null);
        }

        if ( emailsenderedittxt.getText().toString().length()>0) {
            emailsenderedittxt.setError(null);
        }

        if ( pwdedittxt.getText().toString().length()>0) {
            pwdedittxt.setError(null);
        }



        if ( confirmpwdedittxt.getText().toString().length()>0) {
            confirmpwdedittxt.setError(null);
        }

        if (fnshippingedittxt.getText().toString().length()>0) {
            fnshippingedittxt.setError(null);
        }

        if (lnshippingedittxt.getText().toString().length()>0) {
            lnshippingedittxt.setError(null);
        }

        if ( addressshippingedittxt.getText().toString().length()>0) {
            addressshippingedittxt.setError(null);
        }

        if (contactnosshippingedittxt.getText().toString().length()>0) {
            contactnosshippingedittxt.setError(null);
        }

        if ( cityshippingedittxt.getText().toString().length()>0) {
            cityshippingedittxt.setError(null);
        }

        if ( stateshippingedittxt.getText().toString().length()>0) {
            stateshippingedittxt.setError(null);
        }

        if (pincodeshippingedittxt.getText().toString().length()>0) {
            pincodeshippingedittxt.setError(null);
        }

        /*if ( dialogemail.getText().toString().length()>0) {
            dialogemail.setError(null);
        }*/

       /* if ( dialogpwd.getText().toString().length()>0) {
            dialogpwd.setError(null);
        }*/

      /*  if (forgotpwddialogemail.getText().toString().length()>0) {
            forgotpwddialogemail.setError(null);
        }*/


    }

    @Override
    public void afterTextChanged(Editable s) {

    }



     //For Tabs
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_toolbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);




        SearchManager searchManager = (SearchManager) CheckoutActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(CheckoutActivity.this.getComponentName()));
            handleIntent(getIntent());
        }
        return true;
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Log.d("query....", "query...." + query);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Typeface face = Typeface.createFromAsset(getApplication().getAssets(),"fonts/OpenSans-Regular.ttf");    //  THIS



        if (id == R.id.action_login) {

            Intent intent = new Intent(CheckoutActivity.this, Login.class);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.action_terms) {
            Intent in = new Intent(this, TermsandCondition.class);
            startActivity(in);
        }


        return super.onOptionsItemSelected(item);


    }

    private void  login(){

	/*pdia = new ProgressDialog(HomeScreenActivity16.this);
	pdia.setMessage("Please Wait...");
	pdia.setCanceledOnTouchOutside(false);
	pdia.show();
	*/


        StringRequest stringRequest = new StringRequest(Request.Method.GET,SingletonActivity.API_URL+"api/login.php?email="+strdialogmailedittxt+"&&hash="+ strdialogpwdedittxt,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("RESPONSE in Checkout Activity is"+ response);


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("JSONOBJECT response is" +jsonObject);

                            String isSuccessstr = jsonObject.getString("isSuccess");
                            System.out.println("IS SUCCESS STRING IS----" +isSuccessstr);

                            JSONArray jsonArray = jsonObject.getJSONArray("result");
                            System.out.println("JSON ARRAY IS----" +jsonArray);

                            JSONObject mainObject = jsonArray.getJSONObject(0);
                            System.out.println("MAIN OBJECT IS----" +mainObject);

                            String custid = mainObject.getString("customer_id");
                            String custname = mainObject.getString("customer_name");
                            String custemail = mainObject.getString("customer_email");

                            System.out.println("CUSTOMER ID IS----" +custid);
                            System.out.println("CUSTOMER NAME IS----" + custname);
                            System.out.println("CUSTOMER EMAIL IS----" + custemail);

                            dialogcustnamestr = custname;

                            SingletonActivity.custidstr=custid;
                            SingletonActivity.custnamestr=custname;
                            SingletonActivity.custemailstr=custemail;



                            System.out.println("Dialog Cust name is in Checkoutactivity" +dialogcustnamestr);

                            if( dialogcustnamestr!=null) {
                                String[] parts =  dialogcustnamestr.split(" ");
                                String customfirstnme = parts[0]; // 004
                                String customlastnme = parts[1]; // 034556
                                System.out.println("PART 1-----" + customfirstnme);
                                System.out.println("PART 2-----" + customlastnme);


                                fnsenderedittxt.setText(customfirstnme);
                                lnsenderedittxt.setText(customlastnme);


                                emailsendertxtinput.setVisibility(View.GONE);
                                checkBox.setVisibility(View.GONE);
                                alreadyregistxt.setVisibility(View.GONE);
                                loginbtn.setVisibility(View.GONE);


                            }




                            if(isSuccessstr.equalsIgnoreCase("Success!"))
                            {
                                mDialog.dismiss();
                            }


                            if(isSuccessstr.equalsIgnoreCase("no records!"))
                            {
                                Toast.makeText(getBaseContext(), "wrong mailid or password", Toast.LENGTH_SHORT).show();
                            }





                    } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(HomeScreenActivity166.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){


                Map<String,String> params = new HashMap<String, String>();
                params.put("email",strdialogmailedittxt);
                params.put("hash", strdialogpwdedittxt);



                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }






}





